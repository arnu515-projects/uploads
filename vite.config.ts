import { sveltekit } from '@sveltejs/kit/vite';
import { type UserConfig, searchForWorkspaceRoot } from 'vite';

const config: UserConfig = {
	plugins: [sveltekit()],
	server: {
		fs: {
			allow: [searchForWorkspaceRoot(process.cwd()), 'config.ts']
		}
	}
};

export default config;
