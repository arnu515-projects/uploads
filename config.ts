import extToMime from './src/lib/util/extToMime.json';

export const name = 'Media Uploads';

export const description = 'Upload files';

export const allowAnonymousUploads = false;

// allowed values: email , phone
export const authMethod: 'email' | 'phone' = 'email';

// don't include the leading dot (use `png` instead of `.png`)
// leave empty to disable
// extensionless files (e.g. `file`) will be allowed only if their full name is in this list
export const allowedExtensions = [
	'jpg',
	'jpeg',
	'png',
	'svg',
	'webp',
	'gif',
	'mp4',
	'mov',
	'mkv',
	'avi',
	'mp3',
	'wav',
	'ogg',
	'pdf'
];

// set this to false if don't want to check the extension of the file
export const checkExtensions = true;

// convert extension to mime type
// Don't change this, unless you know what you're doing
// All the extensions in the `allowedExtensions` array will be converted to their mime types

export const allowedMimeTypes = [
	...new Set(
		allowedExtensions.map((ext) => {
			return extToMime[ext as keyof typeof extToMime] || 'application/octet-stream';
		})
	)
];

// set this to false if you don't want to check the mime-types of the files
export const checkMimeTypes = true;
