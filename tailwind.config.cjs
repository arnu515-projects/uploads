/** @type {import('tailwindcss').Config} */
const config = {
	content: ['./src/**/*.{html,js,svelte,ts}'],
	darkMode: 'class',
	theme: {
		extend: {
			fontFamily: {
				sans: ['"Baloo 2Variable"', 'sans-serif'],
				serif: ['"Baloo 2Variable"', 'serif']
			}
		}
	},
	plugins: [require('@tailwindcss/forms'), require('daisyui')],
	daisyui: {
		themes: ['winter', 'dracula']
	}
};

module.exports = config;
