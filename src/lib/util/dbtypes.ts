export type Json =
  | string
  | number
  | boolean
  | null
  | { [key: string]: Json }
  | Json[]

export interface Database {
  public: {
    Tables: {
      folders: {
        Row: {
          id: string
          uploader_id: string
          created_at: string
        }
        Insert: {
          id: string
          uploader_id: string
          created_at?: string
        }
        Update: {
          id?: string
          uploader_id?: string
          created_at?: string
        }
      }
      uploads: {
        Row: {
          id: string
          file_name: string
          uploader_id: string
          created_at: string
          folder_id: string
          file_type: string
        }
        Insert: {
          id: string
          file_name: string
          uploader_id: string
          created_at?: string
          folder_id: string
          file_type: string
        }
        Update: {
          id?: string
          file_name?: string
          uploader_id?: string
          created_at?: string
          folder_id?: string
          file_type?: string
        }
      }
    }
    Views: {
      [_ in never]: never
    }
    Functions: {
      [_ in never]: never
    }
    Enums: {
      [_ in never]: never
    }
  }
}
