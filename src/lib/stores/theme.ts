import { get, writable } from 'svelte/store';

export type Theme = 'light' | 'dark' | 'default';

export const theme = writable<Theme>('default');

export const themes = {
	default: 'dracula',
	light: 'winter',
	dark: 'dracula'
};

export function applyTheme(t: Theme) {
	console.log(t);
	const root = document.documentElement;
	root.classList.remove('light', 'dark');
	root.classList.add(t === 'default' ? 'dark' : t);
	root.dataset.theme = themes[t];
	localStorage.setItem('theme', t);
}

export function listenForThemeChanges() {
	const th = localStorage.getItem('theme') || 'default';
	console.log({ th });
	if (['light', 'dark', 'default'].includes(th)) applyTheme(th as Theme);
	theme.set(th as Theme);
	return theme.subscribe(applyTheme);
}
