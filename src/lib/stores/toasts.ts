import { writable } from 'svelte/store';

export interface Toast {
	id: string;
	title: string;
	message: string;
	type: 'info' | 'success' | 'warning' | 'error';
	className?: string;
}

export const toasts = writable<Toast[]>([]);
export const toastTimeout = writable<{ [key: Toast['id']]: NodeJS.Timeout }>({});

export const TOAST_ICONS = {
	info: `<svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" style="width: 30px; height: 30px;">
    <path stroke-linecap="round" stroke-linejoin="round" d="M11.25 11.25l.041-.02a.75.75 0 011.063.852l-.708 2.836a.75.75 0 001.063.853l.041-.021M21 12a9 9 0 11-18 0 9 9 0 0118 0zm-9-3.75h.008v.008H12V8.25z" />
  </svg>`,
	success: `<svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" style="width: 30px; height: 30px;">
    <path stroke-linecap="round" stroke-linejoin="round" d="M9 12.75L11.25 15 15 9.75M21 12a9 9 0 11-18 0 9 9 0 0118 0z" />
  </svg>`,
	warning: `<svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5} stroke="currentColor" style="width: 30px; height: 30px;">
    <path strokeLinecap="round" strokeLinejoin="round" d="M12 9v3.75m9-.75a9 9 0 11-18 0 9 9 0 0118 0zm-9 3.75h.008v.008H12v-.008z" />
  </svg>`,
	error: `<svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" style="width: 30px; height: 30px;">
    <path stroke-linecap="round" stroke-linejoin="round" d="M9.75 9.75l4.5 4.5m0-4.5l-4.5 4.5M21 12a9 9 0 11-18 0 9 9 0 0118 0z" />
  </svg>`
};

export function addToast(toast: Omit<Toast, 'id'>) {
	const id = Math.random().toString(36).substring(2, 9);
	toasts.update((current) => [...current, { id, ...toast }]);

	// remove toast after 5 sec
	toastTimeout.update((current) => {
		current[id] = setTimeout(() => {
			removeToast(id);
		}, 5000);
		return current;
	});
}

export function removeToast(id: Toast['id']) {
	toasts.update((current) => current.filter((t) => t.id !== id));

	// clear timeout if it exists
	toastTimeout.update((current) => {
		if (current[id]) {
			clearTimeout(current[id]);
			delete current[id];
		}
		return current;
	});
}
