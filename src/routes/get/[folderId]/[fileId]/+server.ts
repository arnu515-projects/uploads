import type { RequestHandler } from './$types';
import { getSupabase } from '@supabase/auth-helpers-sveltekit';
import { error } from '@sveltejs/kit';

export const GET: RequestHandler = async (event) => {
	if (
		!/^[0-9a-f]{8}-[0-9a-f]{4}-[0-5][0-9a-f]{3}-[089ab][0-9a-f]{3}-[0-9a-f]{12}$/i.test(
			event.params.fileId
		)
	) {
		throw error(404, 'Not found');
	}

	if (
		!/^[0-9a-f]{8}-[0-9a-f]{4}-[0-5][0-9a-f]{3}-[089ab][0-9a-f]{3}-[0-9a-f]{12}$/i.test(
			event.params.folderId
		)
	) {
		throw error(404, 'Not found');
	}

	const { supabaseClient } = await getSupabase(event);

	const { data: files, error: err } = await supabaseClient
		.from('uploads')
		.select('*')
		.eq('id', event.params.fileId)
		.eq('folder_id', event.params.folderId);

	if (files?.length === 0) {
		throw error(404, 'File not found');
	}

	if (err) {
		console.error('file fetch err', err);
		throw error(500, err.message);
	}

	const file = files[0];

	const { data, error: fileDLError } = await supabaseClient.storage
		.from('files')
		.download(`/${file.uploader_id}/${file.folder_id}/${file.id}`);

	if (fileDLError) {
		console.error('file download err', fileDLError);
		throw error(500, fileDLError.message);
	}

	return new Response(data, { headers: { 'cache-control': 'public, max-age=86400, immutable' } });
};
