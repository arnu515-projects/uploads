import type { PageLoad } from './$types';
import { getSupabase } from '@supabase/auth-helpers-sveltekit';
import { error } from '@sveltejs/kit';

export const load: PageLoad = async (event) => {
	if (
		!/^[0-9a-f]{8}-[0-9a-f]{4}-[0-5][0-9a-f]{3}-[089ab][0-9a-f]{3}-[0-9a-f]{12}$/i.test(
			event.params.fileId
		)
	) {
		throw error(404, 'Not found');
	}

	const { folder } = await event.parent();

	if (!folder) {
		throw error(404, 'Not found');
	}

	const { supabaseClient } = await getSupabase(event);

	const { data: files, error: err } = await supabaseClient
		.from('uploads')
		.select('*')
		.eq('id', event.params.fileId);

	if (err) {
		console.error('file fetch err', err);
		throw error(500, err.message);
	}

	if (files.length === 0) {
		throw error(404, 'Not found');
	}

	return { file: files[0] };
};
