import type { LayoutLoad } from './$types';
import { getSupabase } from '@supabase/auth-helpers-sveltekit';
import { error, redirect } from '@sveltejs/kit';

export const load: LayoutLoad = async (event) => {
	if (
		!/^[0-9a-f]{8}-[0-9a-f]{4}-[0-5][0-9a-f]{3}-[089ab][0-9a-f]{3}-[0-9a-f]{12}$/i.test(
			event.params.folderOrFileId
		)
	) {
		throw error(404, 'Not found');
	}

	const { supabaseClient } = await getSupabase(event);

	const { data: folders, error: err } = await supabaseClient
		.from('folders')
		.select('*')
		.eq('id', event.params.folderOrFileId);

	if (err) {
		console.error('folder fetch err', err);
		throw error(500, err.message);
	}

	if (folders.length === 0) {
		const { data: files, error: err } = await supabaseClient
			.from('uploads')
			.select('id, folder_id')
			.eq('id', event.params.folderOrFileId);

		if (err) {
			console.error('file fetch err', err);
			throw error(500, err.message);
		}

		if (files.length === 0) {
			throw error(404, 'Not found');
		}

		throw redirect(307, `/${files[0].folder_id}/${files[0].id}`);
	}

	return { folder: folders[0] };
};
