import type { PageLoad } from './$types';
import { getSupabase } from '@supabase/auth-helpers-sveltekit';
import { error } from '@sveltejs/kit';

export const load: PageLoad = async (event) => {
	const { folder } = await event.parent();

	if (!folder) {
		throw error(404, 'Not found');
	}

	const { supabaseClient } = await getSupabase(event);

	const { data: files, error: err } = await supabaseClient
		.from('uploads')
		.select('*')
		.eq('folder_id', folder.id);

	if (err) {
		console.error('file fetch err', err);
		throw error(500, err.message);
	}

	if (files.length === 0) {
		throw error(404, 'Not found');
	}

	return { files };
};
