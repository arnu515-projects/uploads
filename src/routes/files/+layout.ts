import { getSupabase } from '@supabase/auth-helpers-sveltekit';
import { redirect } from '@sveltejs/kit';
import type { LayoutLoad } from './$types';

export const load: LayoutLoad = async (event) => {
	const { supabaseClient, session } = await getSupabase(event);

	if (!session) {
		throw redirect(307, '/auth');
	}

	const { data: folders, error } = await supabaseClient
		.from('folders')
		.select('*, uploads (*)')
		.eq('uploader_id', session.user.id);

	if (error) {
		throw new Error(error.message);
	}

	return { folders };
};
