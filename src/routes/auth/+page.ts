import type { PageLoad } from './$types';
import { authMethod } from 'config';

export const load: PageLoad = async () => {
	if (authMethod === 'phone')
		return { countries: (await import('$lib/util/countries.json')).default };
	else return { countries: [] };
};
