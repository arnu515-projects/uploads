import type { PageLoad } from './$types';
import { browser } from '$app/environment';
import { redirect } from '@sveltejs/kit';

export const load: PageLoad = async () => {
	console.log(browser);
	if (browser) {
		const client = (await import('$lib/util/supabase')).default;
		await client.auth.signOut();
		throw redirect(307, '/');
	}
};
